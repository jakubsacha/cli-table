# Rust cli table library

### Usage

##### First steps
In order to start using the cli-table library create the builder object and invoke the build() method on it.

```
let builder = Builder{};
print!(builder.build())
```

### Contributions

This library is going to be written the TDD way; that means:
1. Make current open pull request green.
2. Refactor the code if needed.
3. Merge it to master.
4. Create new pull request with red test.

##### Each pull request should contain:
1. Documentation of the library API if it was modified.
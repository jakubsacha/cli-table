extern crate cli_table;

use cli_table::{Table, Cell, Row};

#[test]
fn it_works() {
    let mut table = Table::new();
    table.add_row(Row::new(vec![Cell::new("hello"), Cell::new("world")]));

    // when
    println!("{}", table.build());
}
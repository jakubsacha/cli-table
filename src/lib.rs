mod entities;
pub use entities::{Table, Row, Cell};

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn given_no_rows_when_executed_result_is_empty() {
        // given
        let table = Table::new();

        // when
        let output = table.build();

        // then
        assert_eq!(output, "\n")
    }

    #[test]
    fn given_one_row_added_when_executed_result_is_valid() {
        // given
        let mut table = Table::new();
        table.add_row("row");

        // when
        let output = table.build();

        // then
        assert_eq!(output, "=====\n|row|\n=====")
    }

    #[test]
    fn given_two_rows_added_when_executed_result_is_valid() {
        // given
        let mut table = Table::new();
        table.add_row("row1");
        table.add_row("row2");

        // when
        let output = table.build();

        // then
        assert_eq!(output, "======\n|row1|\n|row2|\n======")
    }

    #[test]
    fn given_row_struct_added_when_executed_result_is_valid() {
        // given
        let mut table = Table::new();
        table.add_row(Row::new(vec![Cell::new("hello"), Cell::new("world")]));

        // when
        let output = table.build();

        // then
        assert_eq!(output, "=============\n|hello|world|\n=============")
    }
}


pub struct Cell {
    pub value: String,
}

impl Cell {
    pub fn new(value: &str) -> Cell {
        Cell { value: value.into() }
    }
}

impl From<Cell> for String {
    fn from(cell: Cell) -> Self {
        cell.value
    }
}

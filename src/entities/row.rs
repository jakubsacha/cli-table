use entities::cell::Cell;

pub struct Row {
    cells: Vec<Cell>,
}

impl IntoIterator for Row {
    type Item = Cell;
    type IntoIter = ::std::vec::IntoIter<Cell>;

    fn into_iter(self) -> Self::IntoIter {
        self.cells.into_iter()
    }
}

impl Row {
    pub fn new(cells: Vec<Cell>) -> Row {
        Row { cells: cells }
    }
}

impl From<Row> for String {
    fn from(row: Row) -> Self {
        row.into_iter()
            .map(|x| x.value)
            .collect::<Vec<String>>()
            .join("|")
    }
}

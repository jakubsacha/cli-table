pub struct Table {
    separator: u8,
    rows: Vec<String>,
}

impl Table {
    pub fn new() -> Table {
        Table {
            separator: b'=',
            rows: Vec::new(),
        }
    }

    pub fn add_row<T: Into<String>>(&mut self, row: T) {
        self.rows.push(row.into());
    }

    pub fn build(&self) -> String {
        let mut rendered = Vec::new();
        rendered.push(self.header());
        for row in &self.rows {
            rendered.push(format!("|{}|", row).clone());
        }
        rendered.push(self.header());
        rendered.join("\n")
    }

    fn header(&self) -> String {
        let width = self.get_width();
        String::from_utf8(vec![self.separator; width]).unwrap()
    }

    // The width of the table is the width of the longest row.
    // Add two, because of the markup at the beginning and end of each row.
    fn get_width(&self) -> usize {
        match self.rows.len() {
            0 => 0, // No rows, no width
            _ => self.rows.iter().map(|row| row.len()).max().unwrap_or(0) + 2,
        }
    }
}
